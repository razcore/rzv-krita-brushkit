# [RZV Krita BrushKit](https://goo.gl/vWKq1K)

<a href="https://goo.gl/vWKq1K">![rzv-krita-brushkit](https://razcore.art/images/p02-rzv-krita-brushkit/presentation.png)</a>

A brush kit for crispy and experimental digital paintings! Please find more
information on [the project's website](https://razcore.art/posts/2017/06/11/p02-rzv-krita-brushkit/).


## license terms

RZV-KritaBrushKit (c) by "Razvan Radulescu, <https://razcore.art/>".

RZV-KritaBrushKit is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.


What the above means is:

1. the attribution is necessary if redistributing, modifying or using this brush kit in commercial ways
2. the attribution is not necessary in case of usage or screen recording etc. of Krita with the brush kit visible
